var express = require('express');
var app = express();
const sql = require('mssql');

const config = {
    server: "localhost",
    database: "TestDB",
    user: "karinas",
    password: "11111",
    options: {
        trustServerCertificate: true
    }
}

async function getContainers() {
    try {
        var pool = await sql.connect(config)
        let containers = await pool.query("SELECT * from containers")
        console.dir(containers.recordset)
    } catch (err) {
        console.log(err)
    }
}
getContainers()

async function getOperationsContainer() {
    try {
        var pool = await sql.connect(config)
        let operation = await pool.query(`SELECT * from operations where ID_CONT = ${3}`)
        console.dir(operation.recordset)
    } catch (err) {
        console.log(err)
    }
}
getOperationsContainer()


app.get('/containers', (req, res) => {
    sql.connect(config, function () {
        const request = new sql.Request()
        request.query("SELECT * FROM containers", (err, resp) => {
            if (err) {
                console.log('error')
            }
            res.json(resp.recordset);
        })
    })
})

app.get('/operations/:id', (req, res) => {
    sql.connect(config, function () {
        const request = new sql.Request()
        const id = parseInt(req.params.id)
        request.query(`SELECT * from operations where ID_CONT = ${id}`, (err, resp) => {
            if (err) {
                console.log('error')
            }
            res.json(resp.recordset);
        })
    })
})

app.listen(5000, () => {
    console.log("Server started on port 5000")
})